from django.shortcuts import render, redirect

# Create your views here.

from django.http import HttpResponse

# Local Imports
from .models import GroceryItem

# to import built in user model
from django.contrib.auth.models import User

#to use template created
# from django.template import loader

from django.contrib.auth import authenticate, login,logout
def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {
	"groceryitem_list": groceryitem_list,
	"user": request.user
	}

	return render(request, "index.html", context)



def groceryitem(request,groceryitem_id):
	response = f"You are viewing the details of item {groceryitem_id}"
	return HttpResponse(response)



def register(request):

	users = User.objects.all()
	is_user_registered = False

	user = User()
	user.username = "johndoe"
	user.first_name = "john"
	user.last_name = "doe"
	user.email = "john@mail.com"
	# hashed password
	user.set_password("john1234")
	user.is_staff = False
	user.is_active = True

	# for loop to check objects in users list
	for indiv_user in users:
		if indiv_user.username == user.username:
			is_user_registered = True
			break;
		
	# only save when there's no match
	if is_user_registered == False:
		# to save
		user.save()

	context = {
	"is_user_registered": is_user_registered,
	"first_name": user.first_name,
	"last_name": user.last_name

	}

	return render(request,"register.html",context)




def change_password(request):
	is_user_authenticated = False

	user = authenticate(username = "johndoe", password = "john1234")

	if user is not None:
		# get target user
		authenticated_user = User.objects.get(username ="johndoe")
		# setter to change password
		authenticated_user.set_password("john123")
		# save
		authenticated_user.save()

		is_user_authenticated = True


	context = {
	"is_user_authenticated" : is_user_authenticated
	}


	return render(request,"change_password.html",context)






def login_user(request):
	username = "johndoe"
	password = "john123"

	user = authenticate(username = username, password = password)

	if user is not None:
		# param,variable user
		login(request, user)
		# to navigate to index/home page
		return redirect("index")
	else:
		return render(request, "login.html")

		

def logout_user(request):
	logout(request)
	return redirect("index")